<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'typo3/cms-base-distribution',
  ),
  'versions' => 
  array (
    'cogpowered/finediff' => 
    array (
      'pretty_version' => '0.3.1',
      'version' => '0.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '339ddc8c3afb656efed4f2f0a80e5c3d026f8ea8',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce77a7ba1770462cd705a91a151b6c3746f9c6ad',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '13e3381b25847283a91948d04640543941309727',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '2.10.4',
      'version' => '2.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '47433196b6390d14409a33885ee42b6208160643',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'fluidtypo3/fluidpages' => 
    array (
      'pretty_version' => '5.0.0',
      'version' => '5.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ed62c092239a5dcae176bcb80ed931551b540298',
    ),
    'fluidtypo3/flux' => 
    array (
      'pretty_version' => '9.4.2',
      'version' => '9.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6afa4899df6a16ff270736b52cd340b0229246c7',
    ),
    'fluidtypo3/vhs' => 
    array (
      'pretty_version' => '6.0.4',
      'version' => '6.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '56234782ed2e6ad91a7f880ad6107dde5b43f527',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.5',
      'version' => '6.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '60d379c243457e073cff02bc323a2a86cb355631',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '53330f47520498c0ae1f61f7e2c90f55690c06a3',
    ),
    'helhum/class-alias-loader' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'helhum/config-loader' => 
    array (
      'pretty_version' => 'v0.12.2',
      'version' => '0.12.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1faf12146c505f3b929b10f09aabb8b675d7842d',
    ),
    'helhum/typo3-console' => 
    array (
      'pretty_version' => 'v5.8.6',
      'version' => '5.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '49dbcc9183b99878f8f8c91a51fc9dbf04b16f54',
    ),
    'helhum/typo3-console-plugin' => 
    array (
      'pretty_version' => 'v2.0.7',
      'version' => '2.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '08132bd713f338c15009699dfa4bc236f9b5cee9',
    ),
    'lw/typo3cms-installers' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'netresearch/composer-installers' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.4',
      'version' => '4.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6d052fc58cb876152f89f532b95a8d7907e7f0e',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-server-handler' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aff2f80e33b7f026ec96bb42f63242dc50ffcae7',
    ),
    'psr/http-server-middleware' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2296f45510945530b9dceb8bcedb5cb84d40c5f5',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v5.4.12',
      'version' => '5.4.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '181b89f18a90f8925ef805f950d47a7190e9b950',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '23cc546c9104693d6fce1b3aaa31c1fd47198bdc',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v1.1.10',
      'version' => '1.1.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '8d5489c10ef90aa7413e4921fc3c0520e24cbed7',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '12e071278e396cc3e1c149857337e9e192deca0b',
    ),
    'symfony/expression-language' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1763368a38d5061e5aa03160b328075d000291b',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ebd0965f2dc2d4e0f11487c16fbb041e50b5c09b',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '5ebda66b51612516bf338d5f87da2f37ff74cf34',
    ),
    'symfony/intl' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '79c82bb12f88a58afc68ed9b2302ef979df9aa95',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a4176a1cbc4cc99268c531de547fccbd0beb370',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4ba089a5b6366e453971d3aad5fe8e897b37f41',
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c44d5bf6a75eed79555c6bf37505c6d39559353e',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b75acd829741c768bc8b1f84eb33265e7cc5117',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '727d1096295d807c309fb01a851577302394c897',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '39d483bdf39be819deabf04ec872eb0b2410b531',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cede45fcdfabdd6043b3592e83678e42ec69e930',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ff431c517be11c78c48a39a66d37431e26a6bed',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e70aa8b064c5b72d3df2abd5ab1e90464ad009de',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '075316ff72233ce3d04a9743414292e834f2cb4a',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '80b042c20b035818daec844723e23b9825134ba0',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b776d18b303a39f56c63747bcb977ad4b27aca26',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f04b7d187b120e0a44c18a2d479c2dd0abe99d9c',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.4.18',
      'version' => '4.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbce94f14d73732340740366fcbe63363663a403',
    ),
    'typo3-ter/flux' => 
    array (
      'replaced' => 
      array (
        0 => '9.4.2',
      ),
    ),
    'typo3-ter/typo3-console' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.8.6',
      ),
    ),
    'typo3-ter/vhs' => 
    array (
      'replaced' => 
      array (
        0 => '6.0.4',
      ),
    ),
    'typo3/class-alias-loader' => 
    array (
      'pretty_version' => 'v1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '575f59581541f299f3a86a95b1db001ee6e1d2e0',
    ),
    'typo3/cms-about' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb1b5e2de94f481895c19c6ab55b18e5140fe3aa',
    ),
    'typo3/cms-adminpanel' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '22b0aa13d2e331b8afd5d5ff679f4d5f9a9dee29',
    ),
    'typo3/cms-backend' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ada61b805cbf7a9aa2d01e2a63337de2e52d384',
    ),
    'typo3/cms-base-distribution' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'typo3/cms-belog' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ec15fc53b3fcbf12fa35790c42c082527398bd6f',
    ),
    'typo3/cms-beuser' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bc31cae27c92b81e893b51be911c9855ea4d3ec2',
    ),
    'typo3/cms-cli' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '215a0bf5c446b4e0b20f4562bbaf3d6215a5ee82',
    ),
    'typo3/cms-composer-installers' => 
    array (
      'pretty_version' => 'v3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cde93ec5dbfb115e4d47c5f9b6d8822adeeac290',
    ),
    'typo3/cms-context-help' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'typo3/cms-core' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '80663bfb7eabbe33ab6d3c869f49e79501af51fa',
    ),
    'typo3/cms-cshmanual' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'typo3/cms-extbase' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '124b79f921546de75d9aef56a54266dc2774260c',
    ),
    'typo3/cms-extensionmanager' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '66f0d129ca3f82fd3348653753a9c2c89aee2088',
    ),
    'typo3/cms-felogin' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '31613ea3d49f2307aaec959a187fad5e168a4459',
    ),
    'typo3/cms-filelist' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '5bb61d421527b79ea55b7925f9daea6173d4b20d',
    ),
    'typo3/cms-fluid' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bcf0cf0080d0bac7d93c7b59d6a057d24a7f719d',
    ),
    'typo3/cms-fluid-styled-content' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '847a248580f4629712d44ae8ba6d3dc9611e2015',
    ),
    'typo3/cms-form' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'febd8bc04dbf52ca35da53d4a1f6c37effb2546f',
    ),
    'typo3/cms-frontend' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '7ac252275745fa23e3800ff9fb4bb6daa3920762',
    ),
    'typo3/cms-func-wizards' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'typo3/cms-impexp' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '206b4d95a4829c620a9b306f9f70794dea5f482f',
    ),
    'typo3/cms-info' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd0674de93f1eb809da434fc1ce4856fbe96e1fae',
    ),
    'typo3/cms-info-pagetsconfig' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'typo3/cms-install' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '46c8e0a97376a927c04b5f3bf7b02ae02f3c66a1',
    ),
    'typo3/cms-lang' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'typo3/cms-recordlist' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '66d1787c48232933f8f1a9784f583c8d3cfc9f72',
    ),
    'typo3/cms-redirects' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '641a34940b064400f76d9a9c20792ef79020df23',
    ),
    'typo3/cms-reports' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f2e12ca1a6cbaa17becac1558568cc2a4888a3a0',
    ),
    'typo3/cms-rte-ckeditor' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '8441a4629ddb44f2a4b597830d3044ec275bd3df',
    ),
    'typo3/cms-saltedpasswords' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'typo3/cms-scheduler' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '60bc4f98fc366300aaf9553a34016633cfc21405',
    ),
    'typo3/cms-seo' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '31655f43552d134c8c45d881052266a9bcf6abcf',
    ),
    'typo3/cms-setup' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '200ded74853cf83e3dcd9980d7754656f525ee30',
    ),
    'typo3/cms-sv' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'typo3/cms-sys-note' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '49aeea902db36aaaa0c7acfca0d11bfb91f92652',
    ),
    'typo3/cms-t3editor' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '1df3d3b70cca0dd9c6484539b752d050190ffe24',
    ),
    'typo3/cms-tstemplate' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd41995ef06a91a6c8d2c49b42275a67bae4857e',
    ),
    'typo3/cms-viewpage' => 
    array (
      'pretty_version' => 'v9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => '41c61b2074001966f57fdb69349667d5c03abb82',
    ),
    'typo3/cms-wizard-crpages' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'typo3/cms-wizard-sortpages' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'typo3/minimal' => 
    array (
      'pretty_version' => 'v9.5.0',
      'version' => '9.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ca773e3a90f193ef6cbac7be810d2e8eb6d06fc',
    ),
    'typo3/phar-stream-wrapper' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '60131cb573a1e478cfecd34e4ea38e3b31505f75',
    ),
    'typo3fluid/fluid' => 
    array (
      'pretty_version' => '2.6.10',
      'version' => '2.6.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f20db4e74cf9803c6cffca2ed2f03e1b0b89d0dc',
    ),
  ),
);
